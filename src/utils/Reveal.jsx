import React, { useEffect, useRef } from 'react';
import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

const Reveal = ({ children, width = 'fit-content' }) => {
  const ref = useRef(null);
  const controls = useInView({
    triggerOnce: true,
  });

  useEffect(() => {
    if (controls.inView) {
      ref.current.style.visibility = 'visible';
      ref.current.style.opacity = 1;
      ref.current.style.transform = 'translateY(0)';
    }
  }, [controls]);

  return (
    <div style={{ position: 'relative', width, overflow: 'hidden' }}>
      <motion.div
        ref={ref}
        initial={{ opacity: 0, y: 75 }}
        style={{ visibility: 'hidden' }}
        transition={{ duration: 0.5, delay: 0.25 }}
      >
        {children}
      </motion.div>
    </div>
  );
};

export default Reveal;
