import React from 'react'
import Image from 'next/image'

const OneCard = (props) => {
    return (
        <div className="max-w-sm border w-[500px] rounded-lg shadow bg-gray-800 border-gray-700 my-10 mx-auto">
            <a href={props.link} target='_blank' className='w-full h-3/4'>
                <Image src={props.img} alt={props.alt} className='rounded-lg' />
            </a>
            <div className="p-5">
                <a href={props.link} target='_blank'>
                    <h5 className="mb-2 text-2xl font-bold tracking-tight text-white">{props.title}</h5>
                </a>
                <p className='text-white'>{props.desc}</p>
            </div>
        </div>
    )
}

export default OneCard