import React, { useEffect, useState } from 'react'
import OneCard from './OneCard'
import placeholderImage from '../../../public/img/placeholder.jpg'

import axios from 'axios'


const Card = () => {

    const [project, setProject] = useState([])

    useEffect(() => {
        axios.get('https://gitlab.com/api/v4/projects?membership=true', {
            headers: {
                'Authorization': `Bearer ${process.env.NEXT_PUBLIC_APIGIT}`
            }
        })
            .then((response) => {
                const ownerProjects = response.data.filter(
                    (item) => item.owner && item.owner.username === "Reikoarts"
                  );
                setProject(ownerProjects);
                setLoading(false);
            })
            .catch((error) => {
                console.error(error)
        })
    }, [])


    return (
        <div className='w-[100vw] flex flex-col mb-20'>
            <h2 className='text-5xl text-white uppercase mb-10 ml-[10vw]'>Project</h2>
            <div className='w-full grid grid-cols-3 gap-10 text'>
                {project ? project.map((item, index) => {
                    console.log(item)
                    return (
                        <OneCard
                            key={index}
                            title={item.name}
                            img={placeholderImage}
                            desc={item.description || "No description available"}
                            link={item.web_url}
                            alt='Portfolio'
                        />
                    )}) : (
                        <div className='text-white'>Loading...</div>
                    )
                }
            </div>
        </div>
    )
}

export default Card