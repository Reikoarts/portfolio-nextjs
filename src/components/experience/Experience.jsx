import React from 'react'
import gv from '../../../public/img/logogv.svg'
import gh from '../../../public/img/logogh.svg'
import cesi from '../../../public/img/logocesi.svg'
import cloudcampus from '../../../public/img/cloudcampus.png'
import cmcLogo from '../../../public/img/cmc_logo.svg'
import Image from 'next/image'



const Experience = () => {
    return (
        <div className='sm:mt-32'>
            <h2 className='text-5xl text-white uppercase ml-[10vw] lg:mb-32 lg:text-center lg:ml-0'>Experience et formation</h2>
            <div className='h-[100vh] w-[100vw] flex flex-row items-center justify-evenly lg:flex-col'>
                <div>
                    <Image src={cmcLogo} width={400} height={200} alt='Logo de entreprise Groupe Vertus' className='sm:mt-64 sm:w-[300px] sm:m-auto mx-auto'/>
                    <div className='flex flex-col justify-evenly items-center mt-10'>
                        <h2 className='text-4xl text-white mb-3 uppercase sm:text-2xl'>Cross Media Consulting</h2>
                        <p className='text-white sm:text-sm sm:text-center'>Agence Web</p>
                        <p className='text-white sm:text-sm sm:text-center'>Alternant développeur web full stack</p>
                        <p className='text-white sm:text-sm sm:text-center'>3 années d'experiences</p>
                    </div>
                    <div className='flex flex-col justify-evenly items-center mt-10 sm:px-5 sm:gap-2'>
                        <h2 className='text-4xl text-white mb-3 uppercase sm:text-2xl'>Travaux réalisés</h2>
                        <p className='text-white sm:text-sm sm:text-center'>Intégration web</p>
                        <p className='text-white sm:text-sm sm:text-center'>Développement de fonctionnalité custom sur theme wordpress</p>
                        <p className='text-white sm:text-sm sm:text-center'>Intégration Google Ad Manager</p>
                        <p className='text-white sm:text-sm sm:text-center'>Optimisation des performances front-end</p>
                        <p className='text-white sm:text-sm sm:text-center'>Intégration de designs responsives et cross-browser</p>
                        <p className='text-white sm:text-sm sm:text-center'>Intégration de systèmes de paiement</p>
                        <p className='text-white sm:text-sm sm:text-center'>Débogage et correction d’erreurs back-end</p>
                        <p className='text-white sm:text-sm sm:text-center'>Création de thèmes personnalisés (Timber, Twig)</p>
                        <p className='text-white sm:text-sm sm:text-center'>Développement de plugins sur mesure</p>
                        <p className='text-white sm:text-sm sm:text-center'>Intégration d’APIs externes</p>
                        <p className='text-white sm:text-sm sm:text-center'>Gestion des hooks et filtres WordPress</p>
                        <p className='text-white sm:text-sm sm:text-center'>Développement AJAX sur le front et le back</p>
                        <p className='text-white sm:text-sm sm:text-center'>Utilisation de Git/GitHub pour le versioning (GitFlow)</p>
                    </div>

                </div>
                <div className='bg-white w-[5px] h-[80vh] rounded-lg lg:w-[80vw] lg:h-[5px]'></div>
                <div className='lg:flex lg:flex-row lg:justify-evenly lg:items-center lg:w-full lg:mt-20 sm:flex-col'>
                    <div className='flex flex-col justify-evenly items-center mt-10 '>
                        <Image src={cloudcampus} width={200} height={200} alt='Logo de Cloud campus' className='sm:w-[100px] sm:h-[100px]' />
                        <h2 className='text-4xl text-white mb-3 uppercase mt-5 sm:text-2xl'>Cloud Campus</h2>
                        <p className='text-white mb-10 sm:text-sm sm:text-center'>Développeur Web Full Stack</p>

                    </div>
                    <div className='flex flex-col justify-evenly items-center mt-10'><a href="https://gitlab.com/Reikoarts">
                        <Image src={gh} width={200} height={200} alt='Logo GitHub' className='sm:w-[100px] sm:h-[100px]'/>
                        <h2 className='text-4xl text-white mb-3 uppercase mt-5 sm:text-2xl'>Mon Github</h2>
                        </a></div>
                </div>
            </div>
        </div>
    )
}

export default Experience