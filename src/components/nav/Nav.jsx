import React from 'react'

const Nav = () => {
  return (
    <div className='bg-[#1C2942]'>
        <ul className='flex flex-row justify-center items-center gap-8 pt-5 pb-5'>
            <li>
                <a href="/" className='text-[15px] uppercase text-white'>home</a>
            </li>
            <li>
                <a href="/" className='text-[15px] uppercase text-white'>about me</a>
            </li>
            <li>
                <a href="/" className='text-[15px] uppercase text-white'>stack</a>
            </li>
            <li>
                <a href="/" className='text-[15px] uppercase text-white'>project</a>
            </li>
            <li>
                <a href="/" className='text-[15px] uppercase text-white'>experience & training</a>
            </li>
            <li>
                <a href="/" className='text-[15px] uppercase text-white'>contact</a>
            </li>
        </ul>
    </div>
  )
}

export default Nav