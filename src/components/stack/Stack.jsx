import React, { useRef, useEffect } from 'react'
import Image from 'next/image'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'
import html from '../../../public/img/html.svg'
import js from '../../../public/img/js.svg'
import react from '../../../public/img/react.svg'
import next from '../../../public/img/next.svg'
import php from '../../../public/img/php.svg'
import laravel from '../../../public/img/laravel.svg'
import tailwind from '../../../public/img/tailwind.svg'
import wordpress from '../../../public/img/wordpress.svg'
import prisma from '../../../public/img/prisma.svg'



const Stack = () => {

    const sectionRef = useRef(null)
    const triggerRef = useRef(null)

    gsap.registerPlugin(ScrollTrigger)

    useEffect(() => {

        const pin = gsap.fromTo(sectionRef.current, {
            translateX: 0
        }, {
            translateX: "-300vw",
            ease: "none",
            duration: 1,
            scrollTrigger: {
                trigger: triggerRef.current,
                start: "top top",
                end: "2000 top",
                scrub: 1,
                pin: true
            }
        })

        return () => {
            pin.kill()
        }
    }, [])


    return (
        <div className='w-[100vw]'>
            <h2 className='text-5xl text-white uppercase mb-10 ml-[10vw] sm:mt-64'>Stack</h2>
            <div className='overflow-hidden'>
                <div ref={triggerRef}>
                    <div className='snap-x snap-mandatory h-[100vh] w-[400vw] flex flex-row relative justify-between' ref={sectionRef}>
                        <div className='snap-start h-[100vh] w-[100vw] flex flex-col'>
                            <h3 className='text-white text-4xl ml-[10vw] mt-10 uppercase sm:text-center sm:ml-0'>Front-End</h3>
                            <div className='h-[100vh] flex flex-row justify-around items-center sm:flex-col'>
                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>
                                    <Image src={html} width={270} height={270} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>HTML</h3>
                                </div>
                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>
                                    <Image src={js} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>Javascript</h3>
                                </div>
                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>
                                    <Image src={react} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>ReactJs</h3>
                                </div>
                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>
                                    <Image src={next} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />

                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>Nextjs</h3>
                                </div>


                            </div>
                        </div>
                        <div className='snap-start h-[100vh] w-[100vw] flex flex-col'>
                            <h3 className='text-white text-4xl ml-[10vw] mt-10 uppercase sm:text-center sm:ml-0'>Back-End</h3>
                            <div className='h-[100vh] flex flex-row justify-around items-center sm:flex-col'>
                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>
                                    <Image src={php} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px] '>Php</h3>

                                </div>

                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>
                                    <Image src={js} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>Javascript</h3>

                                </div>


                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>

                                    <Image src={laravel} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>Laravel</h3>

                                </div>


                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>

                                    <Image src={next} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>Nextjs</h3>

                                </div>



                            </div>
                        </div>
                        <div className='snap-start h-[100vh] w-[100vw] flex flex-col'>
                            <h3 className='text-white text-4xl ml-[10vw] mt-10 uppercase sm:text-center sm:ml-0'>Extra</h3>
                            <div className='h-[100vh] flex flex-row justify-around items-center sm:flex-col'>
                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>

                                    <Image src={tailwind} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>TailwindCSS</h3>


                                </div>

                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>

                                    <Image src={react} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>ReactNative</h3>


                                </div>

                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>

                                    <Image src={wordpress} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>Wordpress</h3>


                                </div>

                                <div className='relative w-[300px] h-[300px] lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]'>

                                    <Image src={prisma} width={300} height={300} className='lg:w-[150px] lg:h-[150px] sm:w-[100px] sm:h-[100px]' />
                                    <h3 className='absolute bottom-[-50px] left-[50%] translate-x-[-50%] text-white text-4xl uppercase lg:text-xl sm:top-[-30px]'>PrismaJs</h3>


                                </div>


                            </div>
                        </div>
                        <div className='snap-start h-[100vh] w-[100vw] flex flex-col '>
                            <div className='h-[100vh] flex flex-row justify-around items-center'>
                                <h3 className='text-white text-7xl sm:text-5xl'>Et bien d'autres ...</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Stack