import React from 'react'
import head from '../../../public/img/headtest.png'
import Image from 'next/image'
import Reveal from '@/utils/Reveal'

const Hero = () => {
    return (
        <div className='h-[100vh] flex flex-col items-center justify-center'>
                <Image src={head} width={250} height={250} alt="Memoji head of author" />
                <p className='text-xl text-white pt-10'>Salut, je m'appelle Nathan ✌️</p>
                <h1 className='text-3xl text-white pt-5 pb-2'>Développeur web & mobile</h1>


                <h2 className='text-3xl text-white pb-5'>Réalisons ce qui vous ressemble</h2>
                <div className='flex flex-row justify-center items-center w-full gap-10'>
                    <a href="/cv/cv.pdf" className='text-white bg-[#3B556D] px-3 py-3 w-[150px] text-center rounded-lg hover:bg-[#1C2942] transition-colors'>Curriculum Vitae</a>
                </div>

        </div>
    )
}

export default Hero