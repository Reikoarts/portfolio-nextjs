import Image from 'next/image'
import React from 'react'
import body from '../../../public/img/bodytest.png'
import Reveal from '@/utils/Reveal'


const About = () => {
  return (
    <div className='h-[100vh] w-[100vw] mt-32 mb-32 flex flex-row items-center justify-evenly lg:flex-col-reverse'>
        <div className='w-[45vw] gap-5 bg-[#3B556D] px-3 py-3 rounded-lg lg:w-[80vw]'>
            <h2 className='text-5xl text-white uppercase mb-10'>À propos</h2>
            <p className='text-white text-lg xl:text-sm'>Je m'appelle Nathan MACAIGNE, je suis un développeur web passionné et créatif. Mon objectif est de créer des expériences numériques exceptionnelles en combinant les technologies modernes avec un design captivant. Ma maîtrise des langages de programmation tels que HTML, CSS et JavaScript me permet de concevoir des sites web réactifs et conviviaux.<br/>
            </p>
            <p className='text-white text-lg my-5 xl:text-sm'>
            Mon expérience m'a amené à travailler avec des frameworks comme React et Next.js. En utilisant la puissance de React, j'ai créé des interfaces utilisateur dynamiques et interactives, offrant des expériences transparentes aux utilisateurs. Avec Next.js, j'ai également développé des applications rendues côté serveur, améliorant à la fois les performances et le référencement.
            </p>
            <p className='text-white text-lg xl:text-sm'>
            En plus des frameworks Front-End, je suis compétent en développement Back-End, en particulier avec Laravel. En m'appuyant sur ce puissant framework PHP, j'ai construit des API performantes qui facilitent la communication entre les systèmes Front-End et Back-End, tout en assurant la sécurité de mes applications web.
            </p>
        </div>
        <Image src={body} width={400} height={400} alt='Memoji of author' className='lg:w-[200px] lg:h-[200px]'/>
    </div>
  )
}

export default About