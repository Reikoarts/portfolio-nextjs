import Image from 'next/image'
import React from 'react'
import contactI from '../../../public/img/contact.svg'

const Contact = () => {
  return (
    <div className='sm:mt-[200px]'>
      <div className='h-[100vh] w-[100vw] flex flex-row items-center justify-evenly '>
        <div className='hidden'>
          <h2 className='text-5xl text-white uppercase mb-10 sm:text-center'>contact me</h2>
          <form className='hidden flex flex-col items-center justify-center'>
            <input type="text" placeholder='Name' className='bg-[#3B556D] text-white px-3 py-3 w-[30vw] rounded-lg mb-5 md:w-[80vw]' required />
            <input type="email" placeholder='Email' className='bg-[#3B556D] text-white px-3 py-3 w-[30vw] rounded-lg mb-5 md:w-[80vw]' required />
            <input type="object" placeholder='Object' className='bg-[#3B556D] text-white px-3 py-3 w-[30vw] rounded-lg mb-5 md:w-[80vw]' required />
            <textarea placeholder='Message' className='bg-[#3B556D] text-white px-3 py-3 w-[30vw] rounded-lg mb-5 md:w-[80vw]' required />
            <div className='flex flex-row items-center gap-5 w-full'>
              <input type='checkbox' id="condition" name="condition" required />
              <label htmlFor="condition" className='text-white'>I have read and accept the terms and conditions</label>
            </div>
            <button className='text-white bg-[#3B556D] px-3 py-3 w-[150px] rounded-lg mt-5 hover:bg-[#1C2942] transition-colors'>Send</button>

          </form>
        </div>
        <div className='flex flex-col gap-5'>
          <h2 className='text-5xl text-white uppercase mb-10 sm:text-center'>contactez moi</h2>
          <a href="mailto:nathan.macaigne@hotmail.com" className='text-white w-[300px] bg-[#3B556D] px-3 py-3 text-center rounded-lg hover:bg-[#1C2942] transition-colors'>Me contacter par email</a>
          <a href="tel:0670594703" className='text-white bg-[#3B556D] px-3 w-[300px] py-3 text-center rounded-lg hover:bg-[#1C2942] transition-colors'>Me contacter par téléphone</a>
        </div>
        <div className='md:hidden'>
          <Image src={contactI} width={700} height={700} className='xl:w-[550px] xl:h-[550px] lg:w-[350px] lg:h-[350px]'/>
        </div>
      </div>
    </div>
  )
}

export default Contact