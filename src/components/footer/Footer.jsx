import React from 'react'

const Footer = () => {
    return (
        <div className='bg-[#1C2942] px-5 py-5'>
            <p>© All right reserved</p>
        </div>
    )
}

export default Footer