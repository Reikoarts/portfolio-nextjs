import Image from 'next/image'
import { Inter } from 'next/font/google'
import Nav from '../components/nav/Nav'
import Hero from '../components/hero/Hero'
import About from '../components/about/About'
import Experience from '../components/experience/Experience'
import Contact from '@/components/contact/Contact'
import Footer from '@/components/footer/Footer'
import Stack from '@/components/stack/Stack'
import Card from '@/components/card/Card'
import Head from 'next/head'

export default function Home() {
  return (
    <>
      <Head>
        <title>Nathan Macaigne - Développeur Full Stack</title>
        <meta name="description" content="Développeur web full stack maîtrisant Next.js, React.js et Laravel. Je conçois des applications web modernes, performantes et personnalisées, avec une expertise en frontend et backend." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className='bg-[#0B162C]'>
        <Hero />
        <About />
        <Stack />
        <Card />
        <Experience />
        <Contact />
        <Footer />
      </div>
    </>
  )
}
